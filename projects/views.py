from django.shortcuts import render, redirect, get_object_or_404
from .models import Project
from django.contrib.auth.decorators import login_required
from .forms import ProjectForm
from tasks.models import Task

# Create your views here.


@login_required
def project_list(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"list_projects": projects}
    return render(request, "projects/project.html", context)


@login_required
def specific_project(request, id):
    project = get_object_or_404(Project, id=id)
    tasks = Task.objects.all()
    context = {"project_details": project, "tasks": tasks}
    return render(request, "projects/project_detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.author = request.user
            receipt.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "create_new_project": form,
    }
    return render(request, "projects/create_project.html", context)
