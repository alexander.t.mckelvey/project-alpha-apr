from django.urls import path
from .views import project_list, create_project, specific_project

urlpatterns = [
    path("", project_list, name="list_projects"),
    path("<int:id>/", specific_project, name="show_project"),
    path("create/", create_project, name="create_project"),
]
