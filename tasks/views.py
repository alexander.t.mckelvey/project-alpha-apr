from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from .models import Task
from .forms import TaskForm

# Create your views here.


@login_required
def task_list(request):
    task = Task.objects.filter(assignee=request.user)
    context = {"task_list": task}
    return render(request, "task_list.html", context)


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "create_new_task": form,
    }
    return render(request, "create_task.html", context)
